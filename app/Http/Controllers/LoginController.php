<?php

namespace App\Http\Controllers;

use App\Models\Sample_datas;
use Illuminate\Http\Response;
use App\Models\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use App\Models\User_infos;
use Illuminate\Support\Facades\DB;
use Validator;


class LoginController extends Controller
{
    public function checklogin(Request $request)
    {
        
        $password_decode = $request->password;
        $email_decode = $request->email;

        $email = Users::where([
            ['email', '=', $request->email]
        ])->first();

        if ($email != null) {
            $password_decrypt = Crypt::decrypt($email->password);
            $email_DB = $email->email;

            Log::info("email from database :" . $email_DB);
            Log::info("pass decrypt :" . $password_decrypt);
            // Log::info("pass decrypt :" . Crypt::decrypt($password_decrypt));
            Log::info("         ");
            Log::info("         ");
            Log::info("         ");


            // return $email_decode;
            if ($email_DB == $email_decode) {
                Log::info("email Match");
                if ($password_decrypt == $password_decode) {
                    Log::info("password Match");
                    
                    return response()->json(
                        true
                    );
                } else {
                    Log::info("password not Match");
                    // return response()->json(
                    //     false
                    // );
                }
            } else {
                Log::info("email not Match");
                // return response()->json(
                //     false
                // );
            }
        }
        else{
            Log::info("ไม่มี");
            // return response()->json(
            //     false
            // );
        }
    }

    function logout(Request $request)
    {
        $request->session()->flush();
        return $request;
    }
}
