<?php

namespace App\Http\Controllers;

use App\Models\Sample_datas;
use App\Models\User_infos;
use App\Models\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Support\Facades\Log;

class SampleController extends Controller
{

    public function index(Request $request)
    {
        $response = response()->json(DB::table('users')
        ->join('user_infos','user_infos.user_id','=','users.id')
        // ->select(
        //     'users.id',
        //     'users.email',
        //     'users.password',
        //     'user_infos.user_id',
        //     'user_infos.first_name',
        //     'user_infos.last_name',
        //     'user_infos.phone_numb',
        //     'user_infos.address'
        // )
        ->get());
        return $response;
    }


    public function create()
    {
        //
    }

    public function store(Request $request)
    {

        $form_data_user = array(
            'password'          =>  Crypt::encrypt($request->password),
            'email'             =>  $request->email,
            'status'            =>  '1',
            'role_id'           =>  '1'

        );
        $User =Users::create($form_data_user);
        $form_data_user_info = array(
            'user_id'           =>  $User->id,
            'first_name'        =>  $request->first_name,
            'last_name'         =>  $request->last_name,
            'phone_numb'        =>  $request->tel,
            'address'           =>  $request->address,
            'company_name'      =>  $request->company_name,
            'salary'           =>  $request->salary,
        );
        Log::info($form_data_user_info);
        
        // User_infos::create($form_data_user_info);
        User_infos::create($form_data_user_info);

        return response()->json(['success' => 'Data Added successfully.']);
    }

    public function show(Request $request)
    {
        $id_get = $request->id;
        $data = Users::join('user_infos','user_infos.user_id','=','users.id')->findOrFail($id_get);
        return $data;
    }

    public function edit(Request $request)
    {
        $id_get = $request->id;
        // Log::info("Findorfail   ");
        // Log::info($id_get);
        $data = Users::join('user_infos','user_infos.user_id','=','users.id')
        ->select(
            'users.id',
            'users.email',
            'users.password',
            'user_infos.user_id',
            'user_infos.first_name',
            'user_infos.last_name',
            'user_infos.phone_numb',
            'user_infos.address'
        )
        ->findOrFail($id_get);
        Log::info(json_encode($data));
        return $data;
    }

    public function update(Request $request)
    {
        Log::info("password :".$request->password);
        $id_get = $request->hidden_id;
        Log::info(json_encode($id_get));
        $form_data = array(
            'password'          =>  Crypt::encrypt($request->password),
            'email'             =>  $request->email,
            'status'            =>  '1',
            'role_id'           =>  '1',
            'first_name'        =>  $request->first_name,
            'last_name'         =>  $request->last_name,
            'phone_numb'        =>  $request->tel,
            'address'           =>  $request->address,
            'company_name'      =>  $request->company_name,
            'salary'           =>  $request->salary,

        );

        Log::info(json_encode($form_data));
        // Log::info($request->id);
        Log::info($id_get);

        Users::join('user_infos','user_infos.user_id','=','users.id')->whereId($id_get)->update($form_data);

        
        return response()->json(['success' => 'Data is successfully updated']);
    }

    public function destroy(Request $request)
    {
        $id_get = $request->id;
        Log::info("delete   ");
        Log::info($id_get);
        $data = Users::join('user_infos','user_infos.user_id','=','users.id')->findOrFail($id_get);
        // $data = Sample_datas::findOrFail($id);
        $data->delete();
    }
}
