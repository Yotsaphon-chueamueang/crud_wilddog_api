<?php

namespace App\Models;

use App\Models\User_info as user_data;
use Illuminate\Database\Eloquent\Model;

class User_infos extends Model
{
    protected $fillable = [
        'first_name', 
        'last_name',
        'phone_numb',
        'address',
        'company_name',
        'salary',
       ];
}
