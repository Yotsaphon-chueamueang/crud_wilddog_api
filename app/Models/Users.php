<?php
use Spatie\Permission\Traits\HasRoles;
namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Users extends Model
{
    // use HasRoles;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email','password','status','role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
}
