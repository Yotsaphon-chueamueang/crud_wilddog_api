git clone ลงมา

เปิด terminal พิมพ์
composer install

สร้าง schema database 
ชื่อ testing	utf8 general ci

เปิด terminal พิมพ์
cp .env.example .env
จะมีไฟล .env

แก้ไขไฟล์ .env
เพิ่ม APP_KEY="32ตัวอักษร+ตัวเลขแล้วแต่จะใส่"
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=testing
DB_USERNAME=root
DB_PASSWORD=
กด save

เปิด terminal พิมพ์
php artisan migrate:refresh --seed

Run ด้วย