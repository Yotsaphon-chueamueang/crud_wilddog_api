<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


use App\Models\Sample_data;

Route::get('/', function () use ($router) {
    return $router->app->version();
});

//CRUD
//CREATE
Route::post('/store', 'SampleController@store');
// READ 
Route::post('/users', 'SampleController@index');
Route::post('/show', 'SampleController@show');
// UPDATE
Route::post('/update', 'SampleController@update');
Route::post('/edit', 'SampleController@edit');
// DELETE
Route::post('/delete', 'SampleController@destroy');

// Login 
Route::post('/sample/checklogin', 'LoginController@checklogin');
// Route::post('/checklogin', 'LoginController@checklogin');
Route::post('/logout', 'LoginController@logout');