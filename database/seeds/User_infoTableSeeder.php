<?php

use App\Models\User_infos;
use Illuminate\Database\Seeder;

class User_infoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User_infos::create([
            'user_id'                   => '1',
            'first_name'                => 'Yotsaphon',
            'last_name'                 => 'Chueamueangphan',
            'phone_numb'                => '0933100909',
            'address'                   => '19/2 asd Bangkok',
            'company_name'              =>  'Digio',
            'salary'                    =>  '50000',
            // 'image'                   => '638233638.jpg',
            // 'remember_token'        => Str::random(10),
            ]);
    }
}
